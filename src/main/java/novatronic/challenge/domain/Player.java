package novatronic.challenge.domain;

public class Player {
    private String playerName;

    public Player(String playerName) {
        this.playerName = playerName;
    }

    public void scorePoint(){
       System.out.println(playerName + " scores");
    }

    public String getPlayerName() {
        return playerName;
    }

}
