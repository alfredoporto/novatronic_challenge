package novatronic.challenge.domain;

public class TennisGame {
    private Player playerOne;
    private Player playerTwo;
    private Score score;

    public TennisGame(Player playerOne, Player playerTwo, Score score) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.score = score;
    }

    public void play(){
        double r = Math.random();
        while(score.hasWinner()){
            if(r > 0.5){
                this.playerOne.scorePoint();
                notifyPoint(1);
            }else{
                this.playerTwo.scorePoint();
                notifyPoint(2);
            }
            getScore();
        }
    }

    private void notifyPoint(int side){
        this.score.updateScore(side);
    }

    private String getScore(){
        if (this.score.hasWinner()){
            return "Game " + getPlayerWithHighestScore();
        }
        if (this.score.hasAdvantage()){
            return "Advantage "+ getPlayerWithHighestScore();
        }
        if (this.score.isDeuce()){
            return "Deuce";
        }
        if(this.score.getPlayerOneScore() == this.score.getPlayerTwoScore()){
            return this.score.translateScore(this.score.getPlayerOneScore()) + " all";
        }
        return this.score.translateScore(this.score.getPlayerOneScore()) + ", " + this.score.translateScore(this.score.getPlayerTwoScore());
    }

    private String getPlayerWithHighestScore() {
        if(this.score.getPlayerOneScore() > this.score.getPlayerTwoScore()){
            return playerOne.getPlayerName();
        }else {
            return playerTwo.getPlayerName();
        }
    }
}
