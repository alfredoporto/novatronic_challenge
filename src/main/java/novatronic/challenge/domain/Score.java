package novatronic.challenge.domain;

public class Score {
    private int playerOneScore;
    private int playerTwoScore;


    public Score() {
        this.playerOneScore = 0;
        this.playerTwoScore = 0;
    }

    public void updateScore(int numberSide) {
        if(numberSide == 1){
            playerOneScores();
        }else {
            playerTwoScores();
        }
    }

    private void playerTwoScores() {
        this.playerTwoScore++;
    }

    private void playerOneScores() {
        this.playerOneScore++;
    }

    public boolean isDeuce() {
        return this.playerOneScore >= 3 && this.playerTwoScore == this.playerOneScore;
    }

    public boolean hasAdvantage() {
        if (playerTwoScore >= 4 && playerTwoScore == playerOneScore + 1){
            return true;
        }
        if (playerOneScore >= 4 && playerOneScore == playerTwoScore + 1) {
            return true;
        }
        return false;
    }

    public String translateScore(int score) {
        switch (score) {
            case 3:
                return "Forty";
            case 2:
                return "Thirty";
            case 1:
                return "Fifteen";
            case 0:
                return "Love";
            default:
                return "Something went wrong...";
        }
    }

    public boolean hasWinner() {
        if(playerTwoScore >= 4 && playerTwoScore >= playerOneScore + 2 ) {
            return true;
        }
        if(playerOneScore >= 4 && playerOneScore >= playerTwoScore + 2) {
            return true;
        }
        return false;
    }

    public int getPlayerOneScore() {
        return playerOneScore;
    }

    public int getPlayerTwoScore() {
        return playerTwoScore;
    }

}
